create database PhoneStation

use PhoneStation

create table ATS
(
    atsId     int not null primary key,
    districtCode      int,
    numbersCount int
)

insert into ATS(atsId, districtCode, numbersCount)
values (1, 501, 50)
insert into ATS(atsId, districtCode, numbersCount)
values (2, 501, 30)
insert into ATS(atsId, districtCode, numbersCount)
values (3, 502, 51)
insert into ATS(atsId, districtCode, numbersCount)
values (4, 502, 100)
insert into ATS(atsId, districtCode, numbersCount)
values (5, 503, 45)
insert into ATS(atsId, districtCode, numbersCount)
values (47, 503, 45)


create table Abonent
(
    abonentId int not null primary key,
    nameSurname nvarchar(100) check (nameSurname like '[A-Z�-߲�]% [A-Z�-߲�]%'),
    address     nvarchar(50) check (Address like '[A-Z�-߲�]%'),
)
insert into Abonent(abonentId, nameSurname, address)
values (1, '������ ͳ�����', '��� ������� 15')
insert into Abonent(abonentId, nameSurname, address)
values (2, '���� ��������', '�������� 13')
insert into Abonent(abonentId, nameSurname, address)
values (3, '���� ��������', '������������ 35')
insert into Abonent(abonentId, nameSurname, address)
values (4, '����� ����������', '���������� 103')
insert into Abonent(abonentId, nameSurname, address)
values (5, '������ �����', '������� 11')


create table Telephone 
(
    tNumber   int not null,
    abonentId     int references Abonent (abonentId),
    atsId          int references ATS (atsId),
    installationDate date,
    mortgage money
)
insert into Telephone (tNumber, abonentId, atsId, installationDate, mortgage)
values (41053, 2, 5, '20200330', 50.00)
insert into Telephone (tNumber, abonentId, atsId, installationDate, mortgage)
values (41057, 1, 3,  '20000911', 35.00)
insert into Telephone (tNumber, abonentId, atsId, installationDate, mortgage)
values (41054, 5, 4, '20200511', 60.00)
insert into Telephone (tNumber, abonentId, atsId, installationDate, mortgage)
values (41055, 4, 2, '20160322', 0.00)
insert into Telephone (tNumber, abonentId, atsId, installationDate, mortgage)
values (41056, 3, 1, '20000420', 0.00)
insert into Telephone (tNumber, abonentId, atsId, installationDate, mortgage)
values (41056, 5, 1, '20000420', 0.00)
insert into Telephone (tNumber, abonentId, atsId, installationDate, mortgage)
values (41056, 3, 47, '20000420', 110.00)
select *
from ATS
select *
from Abonent
select *
from Telephone 
/*1*/
SELECT tel.tNumber, tel.mortgage 
FROM Telephone as tel 
WHERE mortgage > 0
/*2*/
SELECT Count(atsId) AS [atsId], districtCode FROM ATS
GROUP BY districtCode
HAVING (((Count(atsId))>0));
/*3*/
SELECT tel.tNumber, abo.nameSurname FROM Telephone as tel
JOIN Abonent as abo ON tel.abonentId = abo.abonentId
WHERE tel.tNumber = (SELECT tel.tNumber
FROM Telephone tel JOIN Abonent as abo ON tel.abonentId = abo.abonentId
GROUP BY tel.tNumber
HAVING (((Count(abo.abonentId))>1)))
/*4*/
SELECT abo.nameSurname, tel.mortgage
FROM ATS INNER JOIN (Abonent as abo JOIN Telephone as tel ON abo.abonentId = tel.abonentId) ON ATS.atsId = tel.atsId
WHERE (((tel.mortgage)>100) AND ((ATS.atsId)=47));