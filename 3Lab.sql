BACKUP DATABASE Team
TO DISK = 'D:\ThirdCourse\DataBase\Team_TSQL.bak'
 WITH INIT,
 NAME = 'Full Backup of Team',
 DESCRIPTION = 'Team Full Database Backup';

RESTORE DATABASE Team
FROM DISK = 'D:\ThirdCourse\DataBase\Team_TSQL.bak'
WITH RECOVERY, REPLACE

-- ����� ������������ �� ��������
BACKUP DATABASE Team
TO DISK = 'D:\ThirdCourse\DataBase\Team_TSQL2.bak'
WITH INIT, NAME = 'Team Full Database backup',
DESCRIPTION = 'Team Full Database Backup'
-- ������������ �������
BACKUP LOG Team
TO DISK = 'D:\ThirdCourse\DataBase\Team_log.bak'
WITH NOINIT, NAME = 'Team Translog backup',
DESCRIPTION = 'Team Transaction Log Backup', NOFORMAT
-- ������������ ������������ ��������� �������:
BACKUP LOG Team
TO DISK = 'D:\ThirdCourse\DataBase\Team_log.bak'
WITH NORECOVERY

RESTORE DATABASE Team
FROM DISK = 'D:\ThirdCourse\DataBase\Team_TSQL2.bak'
WITH RECOVERY, REPLACE

BACKUP DATABASE Team
TO DISK = 'D:\ThirdCourse\DataBase\Team_diffbkup.bak'
WITH INIT, DIFFERENTIAL, NAME = 'Team Diff Db backup',
DESCRIPTION = 'Team Differential Database Backup'RESTORE DATABASE Team
FROM DISK = 'D:\ThirdCourse\DataBase\Team_diffbkup.bak'
WITH NORECOVERY